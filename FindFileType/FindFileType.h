#pragma once
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <bitset>
#include <iomanip>   

class FindFileType
{
public:
	FindFileType();
	~FindFileType();

	static std::string ReadFileHeader(const std::string filename, int bytesToRead);
	static std::string ToHex(const std::string& s, bool upper_case);
};

