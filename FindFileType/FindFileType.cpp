#include "FindFileType.h"


FindFileType::FindFileType()
{
}


FindFileType::~FindFileType()
{
}

std::string FindFileType::ReadFileHeader(const std::string filename, int bytesToRead)
{
	using namespace std;

	ifstream::pos_type size = bytesToRead;
	char * memblock;

	ifstream file(filename, ios::in | ios::binary | ios::ate);
	if (file.is_open())
	{
		//size = file.tellg();
		memblock = new char[size];
		file.seekg(0, ios::beg);
		file.read(memblock, size);
		file.close();

		cout << "the complete file content is in memory" << endl;
	}

		return std::string(memblock, size);
}

std::string FindFileType::ToHex(const std::string& s, bool upper_case)
	{
		std::ostringstream ret;

		for (std::string::size_type i = 0; i < s.length(); ++i)
		{
			int z = s[i] & 0xff;
			ret << std::hex << std::setfill('0') << std::setw(2) << (upper_case ? std::uppercase : std::nouppercase) << z;
		}

		return ret.str();
	}